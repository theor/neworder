using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using NewOrder.ViewModels;
using Ninject;
using Screen = System.Windows.Forms.Screen;

namespace NewOrder.Services
{
    public class ScreenViewService
    {
        private WindowState _WindowState = WindowState.Minimized;
        private readonly IEventAggregator _EventAggregator;
        [Inject]public IWindowManager WindowManager { get; set; }
        public Profile Profile { get; private set; }


        public ScreenViewService(IEventAggregator eventAggregator)
        {
            Profile = ConfigSerialization.LoadConfig("def.json");
            _EventAggregator = eventAggregator;
            _EventAggregator.Subscribe(this);
            IoC.Get<IEventAggregator>().PublishOnUIThread(new SplitConfigChanged(Profile));
        }

        public void Show()
        {
            _WindowState = WindowState.Normal;
            ScreenViewModels = new List<ScreenViewModel>();
            foreach (var screen in Screen.AllScreens)
            {
                SplitConfig conf;
                if(!Profile.ConfigsByScreen.TryGetValue(screen, out conf))
                    conf = new SplitConfig(null, screen);
                var screenViewModel = new ScreenViewModel(screen, conf);
                ScreenViewModels.Add(screenViewModel);
                WindowManager.ShowWindow(screenViewModel);
            }
        }

        public void Hide()
        {
            _WindowState = WindowState.Minimized;
            foreach (var screenViewModel in ScreenViewModels)
            {
                screenViewModel.TryClose();
            }
        }

        public List<ScreenViewModel> ScreenViewModels { get; set; }
        public void Save(string saveAs)
        {
            Profile.ConfigsByScreen = ScreenViewModels.ToDictionary(svm => svm.Screen, svm => svm.BuildConfig(svm.Screen));
            if (saveAs != null)
                Profile.Name = saveAs;
            ConfigSerialization.SaveConfigs(Profile);
            Hide();
            //ConfigSerialization.SaveConfigs(dict);
            _EventAggregator.PublishOnUIThread(new SplitConfigChanged(Profile));
        }

        public void ShowOrHide()
        {
            if (_WindowState == WindowState.Minimized)
                Show();
            else
                Hide();
        }

        public void Load(string profileToLoad)
        {
            Profile = ConfigSerialization.LoadConfig(profileToLoad);
            IoC.Get<IEventAggregator>().PublishOnUIThread(new SplitConfigChanged(Profile));
            Hide();
            Show();
        }

        public void CycleToProfile(bool toNext)
        {
            var profiles = ConfigSerialization.ListProfiles();
            var i = Array.IndexOf(profiles, ".\\" + Profile.Name + ".json") + 1;
            Load(profiles[i % profiles.Length]);
        }
    }
}