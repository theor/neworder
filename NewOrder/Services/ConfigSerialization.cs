using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Caliburn.Micro;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Screen = System.Windows.Forms.Screen;

namespace NewOrder.Services
{
    public static class ConfigSerialization
    {
        private static readonly JsonSerializerSettings _JsonSerializerSettings = new JsonSerializerSettings
        {
            Formatting = Formatting.Indented,
            Converters = new List<JsonConverter> { new StringEnumConverter() }
        };

        public static Profile LoadConfig(string file)
        {
            var rootPath = IoC.Get<UpdaterService>().InstallPath;
            file = Path.Combine(rootPath, file);
            if (!File.Exists(file))
            {
                var defaultProfile = new Profile("def", Screen.AllScreens.ToDictionary(s => s, s => new SplitConfig(null, s)));
                SaveConfigs(defaultProfile);
                return defaultProfile;
            }
            var toSerDict = JsonConvert.DeserializeObject<Dictionary<string, SplitConfig>>(
                File.ReadAllText(file), _JsonSerializerSettings);
            Dictionary<string, Screen> dScreen = Screen.AllScreens.ToDictionary(ScreenIndex);
            var res = new Dictionary<Screen, SplitConfig>();
            foreach (KeyValuePair<string, SplitConfig> p in toSerDict)
            {
                Screen s;
                if (!dScreen.TryGetValue(p.Key, out s))
                    continue;
                p.Value.Screen = s;
                res.Add(s, p.Value);
            }
            return new Profile(Path.GetFileNameWithoutExtension(file), res);
        }

        public static void SaveConfigs(Profile profile)
        {
            Dictionary<string, SplitConfig> toSerDict = profile.ConfigsByScreen.ToDictionary(kvp => ScreenIndex(kvp.Key), kvp => kvp.Value);

            string json = JsonConvert.SerializeObject(toSerDict, _JsonSerializerSettings);

            var rootPath = IoC.Get<UpdaterService>().InstallPath;
            string file = Path.Combine(rootPath, profile.Name + ".json");
            File.WriteAllText(file, json);
        }

        private static string ScreenIndex(Screen screen)
        {
            return Regex.Match(screen.DeviceName, "(?<id>\\d+)").Groups["id"].Value;
        }

        public static string[] ListProfiles()
        {
            return Directory.GetFiles(".", "*.json");
        }
    }
}