using System;
using System.Drawing;
using System.Reflection;
using System.Windows;
using System.Windows.Forms;
using Caliburn.Micro;
using NewOrder.ViewModels;
using Ninject;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace NewOrder.Services
{
    public class NotifyIconService : IDisposable
    {
        private NotifyIcon _NIcon;

        [Inject]
        public IEventAggregator EventAggregator { get; set; }
        [Inject]
        public ScreenViewService ScreenViewService { get; set; }

        public void Start()
        {
            var currentVersion = IoC.Get<UpdaterService>().CurrentVersion;
            _NIcon = new NotifyIcon
            {
                Icon = new Icon("Icon.ico"),
                Visible = true,
                Text = "New Order " + currentVersion,
                ContextMenu = new ContextMenu(new[]
                {
                    new MenuItem("Help", (sender, args) => IoC.Get<IWindowManager>().ShowPopup(IoC.Get<HelpViewModel>())),
                    new MenuItem("Exit", (sender, args) => Application.Current.Shutdown()),
                    new MenuItem("About", (sender, args) => MessageBox.Show(string.Format("New Order v{0}", currentVersion))),
                    new MenuItem("Check for updates", (sender, args) => IoC.Get<UpdaterService>().CheckForUpdate()),
                })
            };
            _NIcon.MouseClick += (sender, args) =>
            {
                if (args.Button == MouseButtons.Left)
                {
                    HandleLeftClick();
                }
            };

        }

        public void Dispose()
        {
            _NIcon.Dispose();
        }

        private void HandleLeftClick()
        {
            ScreenViewService.ShowOrHide();
        }
    }

    public class NotifyIconMessage
    {
        public WindowState WindowState { get; set; }

        public NotifyIconMessage(WindowState windowState)
        {
            WindowState = windowState;
        }
    }
}