using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows;
using Caliburn.Micro;
using NewOrder.Hotkey;
using Ninject;

namespace NewOrder.Services
{
    public class HotKeyService : IHandle<SplitConfigChanged>
    {


        public const Modifiers Modifiers = Hotkey.Modifiers.Ctrl | Hotkey.Modifiers.Win;

        [Inject]
        public ScreenViewService ScreenViewService { get; set; }

        private int _CurIndex;
        private List<Hotkey.Hotkey> _HotKeys;
        private Profile _Profile;
        public readonly Dictionary<Keys, Action<IntPtr, List<SplitConfig>>> Actions;

        public HotKeyService()
        {
            IoC.Get<IEventAggregator>().Subscribe(this);
            Actions = new Dictionary<Keys, Action<IntPtr, List<SplitConfig>>>
            {
                {Keys.Right, NextZone},
                {Keys.Left, PrevZone},

                {Keys.Up, MaximizeWindow},
                {Keys.Down, MinimizeWindow},

                {Keys.PageUp, NextProfile},
                {Keys.PageDown, PrevProfile},

                {Keys.Home, ShowOrHide},
                {Keys.End, (w, leaves) => { }},
            };
        }

        [Description("Show or hide zone editor")]
        private void ShowOrHide(IntPtr w, List<SplitConfig> leaves)
        {
            ScreenViewService.ShowOrHide();
        }

        [Description("Cycle to previous profile")]
        private void PrevProfile(IntPtr w, List<SplitConfig> leaves)
        {
            IoC.Get<ScreenViewService>().CycleToProfile(false);
        }

        [Description("Cycle to next profile")]
        private void NextProfile(IntPtr w, List<SplitConfig> leaves)
        {
            IoC.Get<ScreenViewService>().CycleToProfile(true);
        }

        [Description("Minimize current window")]
        private void MinimizeWindow(IntPtr w, List<SplitConfig> leaves)
        {
            User32.ShowWindow(w, User32.WindowShowStyle.ShowMinimized);
        }

        [Description("Maximize current window")]
        private void MaximizeWindow(IntPtr w, List<SplitConfig> leaves)
        {
            User32.ShowWindow(w, User32.WindowShowStyle.ShowMaximized);
        }

        [Description("Move current window to previous zone")]
        private void PrevZone(IntPtr w, List<SplitConfig> leaves)
        {
            _CurIndex = (_CurIndex - 1 + leaves.Count) % leaves.Count;
            var cur = leaves[_CurIndex];
            User32.ShowWindow(w, User32.WindowShowStyle.Restore);
            SplitConfigExt.MoveWindow(cur, w);
        }

        [Description("Move current window to next zone")]
        private void NextZone(IntPtr w, List<SplitConfig> leaves)
        {
            _CurIndex = (_CurIndex + 1) % leaves.Count;
            var cur = leaves[_CurIndex];
            User32.ShowWindow(w, User32.WindowShowStyle.Restore);
            SplitConfigExt.MoveWindow(cur, w);
        }

        public void Start()
        {
            // notify icon container
            Window window = new Window { Width = 20, Height = 20 };
            window.Show();
            window.Visibility = Visibility.Collapsed;

            _HotKeys = new List<Hotkey.Hotkey>();
            foreach (var pair in Actions)
            {
                var hotkey = new Hotkey.Hotkey(Modifiers, pair.Key, window, true);
                hotkey.HotkeyPressed += (sender, args) => _HotKey_HotkeyPressed(pair.Key, pair.Value);
                _HotKeys.Add(hotkey);
            }
        }

        public void Dispose()
        {
            IoC.Get<IEventAggregator>().Unsubscribe(this);

            if (_HotKeys == null)
                return;
            foreach (var hotkey in _HotKeys)
            {
                if (hotkey == null)
                    continue;
                try
                {
                    hotkey.Dispose();
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e);
                }
            }
            _HotKeys = null;
        }

        private void _HotKey_HotkeyPressed(Keys keys, Action<IntPtr, List<SplitConfig>> action)
        {
            if (_Profile == null)
                return;
            IntPtr w = User32.GetForegroundWindow();
            Trace.WriteLine(User32.GetActiveWindowTitle());
            //Screen s = Screen.FromHandle(w);

            var all = _Profile.ConfigsByScreen.Values.SelectMany(x => x.GetAll()).ToList();
            //var allO = confs.Values.SelectMany(x => x.GetAll()).OrderBy(x => x.Id).ToList();
            List<SplitConfig> leaves = all.Where(x => x.Direction == SplitConfig.EDirection.None).ToList();

            action(w, leaves);
        }

        public void Handle(SplitConfigChanged message)
        {
            _Profile = message.Profile;
        }
    }

    public struct SplitConfigChanged
    {
        public Profile Profile { get; private set; }

        public SplitConfigChanged(Profile profile)
            : this()
        {
            Profile = profile;
        }
    }
}