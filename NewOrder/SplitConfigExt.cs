using System;
using System.Collections.Generic;
using System.Linq;
using NewOrder.Hotkey;

namespace NewOrder
{
    public static class SplitConfigExt
    {
        public static IEnumerable<SplitConfig> GetAll(this SplitConfig conf)
        {
            yield return conf;
            switch (conf.Direction)
            {
                case SplitConfig.EDirection.None:
                    yield break;
                case SplitConfig.EDirection.Horizontal:
                case SplitConfig.EDirection.Vertical:
                    foreach (var c in conf.A.GetAll().Concat(conf.B.GetAll()))
                        yield return c;
                    break;
            }
        }

        public static void MoveWindow(SplitConfig splitConfig, IntPtr window)
        {
            User32.SetWindowPos(window, splitConfig.Left, splitConfig.Top, splitConfig.Width, splitConfig.Height);
        }
    }
}