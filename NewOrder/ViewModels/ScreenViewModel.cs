using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Caliburn.Micro;
using NewOrder.Services;

namespace NewOrder.ViewModels
{
    public class ScreenViewModel : Screen, IHandle<NotifyIconMessage>, IParent
    {
        public readonly System.Windows.Forms.Screen Screen;
        private ZoneViewModel _Zone;
        private WindowState _WindowState = WindowState.Normal;
        private Visibility _Visibility = Visibility.Visible;

        public PrimaryControlsViewModel PrimaryControls { get; set; }
        public NotifyIconService NotifyIconService { get; set; }
        public string ScreenId { get { return Screen.DeviceName; } }

        public ZoneViewModel Zone
        {
            get { return _Zone; }
            set
            {
                _Zone = value;
                NotifyOfPropertyChange();
            }
        }

        public ScreenViewModel(System.Windows.Forms.Screen screen, SplitConfig conf)
        {
            if (screen.Primary)
            {
                PrimaryControls = new PrimaryControlsViewModel();
            }
            Screen = screen;
            DisplayName = ScreenId;
            var ev = IoC.Get<IEventAggregator>();
            ev.Subscribe(this);

            if (conf != null)
                Zone = LoadZones(conf, this);
        }

        private ZoneViewModel LoadZones(SplitConfig conf, IParent parent)
        {
            ZoneViewModel zone;
            switch (conf.Direction)
            {
                case SplitConfig.EDirection.Horizontal:
                    zone = new HorizontalZoneViewModel(conf.SizeA, conf.SizeB, conf.Id, parent);
                    break;
                case SplitConfig.EDirection.Vertical:
                    zone = new VerticalZoneViewModel(conf.SizeA, conf.SizeB, conf.Id, parent);
                    break;
                default:
                    zone = new LeafZoneViewModel(conf.Id, parent);
                    break;
            }
            if (conf.A != null && conf.B != null)
            {
                zone.Zone1 = LoadZones(conf.A, zone);
                zone.Zone2 = LoadZones(conf.B, zone);
            }
            return zone;
        }

        public void OnKeyDown(KeyEventArgs args)
        {
            if (args.Key == Key.Escape)
                IoC.Get<ScreenViewService>().Hide();
        }

        protected override void OnViewAttached(object view, object context)
        {
            Window w = view as Window;
            if (w != null)
            {
                w.AllowsTransparency = true;
                SolidColorBrush scb = w.Background as SolidColorBrush;
                if(scb != null)
                    w.Background = new SolidColorBrush(Color.FromArgb(200, scb.Color.R, scb.Color.G, scb.Color.B));
            }
            base.OnViewAttached(view, context);
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
        }

        //public override object GetView(object context = null)
        //{
        //    var view = base.GetView(context);
        //    return view;
        //}

        public void OnLoaded(Window w)
        {
            w.Width = Screen.WorkingArea.Width;
            w.Height = Screen.WorkingArea.Height;
            w.Left = Screen.WorkingArea.X;
            w.Top = Screen.WorkingArea.Y;
        }

        public SplitConfig BuildConfig(System.Windows.Forms.Screen screen)
        {
            return Zone.BuildConfig(null, screen);
        }

        public void Handle(NotifyIconMessage message)
        {
            switch (message.WindowState)
            {
                case WindowState.Minimized:
                    break;
                default:
                    break;
            }
            //this.WindowState = message.State;
            //if(message.State == WindowState.Minimized)
            //    Visibility = Visibility.Collapsed;
            //else
            //    Visibility = Visibility.Visible;
        }

        public WindowState WindowState
        {
            get { return _WindowState; }
            set
            {
                _WindowState = value;
                NotifyOfPropertyChange();
            }
        }

        public Visibility Visibility
        {
            get { return _Visibility; }
            set
            {
                if (value == _Visibility) return;
                _Visibility = value;
                NotifyOfPropertyChange();
            }
        }

        public void ReplaceWithSplit(ZoneViewModel zone, SplitConfig.EDirection direction)
        {
            Zone = direction == SplitConfig.EDirection.Horizontal
                       ? (ZoneViewModel)new HorizontalZoneViewModel(1, 1, -1, this)
                       : new VerticalZoneViewModel(1, 1, -1, this);

            Zone.Zone1 = new LeafZoneViewModel(-1, Zone);
            Zone.Zone2 = new LeafZoneViewModel(-1, Zone);
        }

        public void ReplaceWith(ZoneViewModel replaced, ZoneViewModel newOne)
        {
            Trace.Assert(Zone == replaced);
            Zone = newOne;
            newOne.Parent = this;
        }
    }
}