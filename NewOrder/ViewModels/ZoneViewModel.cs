using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Caliburn.Micro;
using Screen = System.Windows.Forms.Screen;

namespace NewOrder.ViewModels
{
    public interface IParent
    {
        void ReplaceWithSplit(ZoneViewModel zone, SplitConfig.EDirection direction);
        void ReplaceWith(ZoneViewModel replaced, ZoneViewModel newOne);
    }

    public abstract class ZoneViewModel : PropertyChangedBase, IParent
    {
        private readonly SplitConfig.EDirection _Direction;
        public SplitConfig.EDirection Direction
        {
            get { return _Direction; }
        }

        public int Id { get; set; }

        public ZoneViewModel Zone1
        {
            get { return _Zone1; }
            set
            {
                if (Equals(value, _Zone1)) return;
                if (value != null)
                    value.Parent = this;
                _Zone1 = value;
                NotifyOfPropertyChange();
            }
        }

        public ZoneViewModel Zone2
        {
            get { return _Zone2; }
            set
            {
                if (Equals(value, _Zone2)) return;
                if (value != null)
                    value.Parent = this;
                _Zone2 = value;
                NotifyOfPropertyChange();
            }
        }

        private Grid _grid;

        private double _Size1;
        public double Size1
        {
            get { return _Size1; }
            set
            {
                if (value == _Size1) return;
                _Size1 = value;
                NotifyOfPropertyChange();
            }
        }

        private double _Size2;
        public double Size2
        {
            get { return _Size2; }
            set
            {
                if (value == _Size2) return;
                _Size2 = value;
                NotifyOfPropertyChange();
            }
        }

        public IParent Parent;
        private ZoneViewModel _Zone1;
        private ZoneViewModel _Zone2;

        protected ZoneViewModel(int id, SplitConfig.EDirection direction, double size1, double size2, IParent parent)
        {
            Parent = parent;
            Id = id;
            _Direction = direction;
            _Size1 = size1;
            _Size2 = size2;
        }

        public void OnLoaded(Grid g)
        {
            _grid = g;
            switch (Direction)
            {
                case SplitConfig.EDirection.None:
                    break;
                case SplitConfig.EDirection.Horizontal:
                    g.RowDefinitions[0].Height = new GridLength(Size1, GridUnitType.Star);
                    g.RowDefinitions[2].Height = new GridLength(Size2, GridUnitType.Star);
                    break;
                case SplitConfig.EDirection.Vertical:
                    g.ColumnDefinitions[0].Width = new GridLength(Size1, GridUnitType.Star);
                    g.ColumnDefinitions[2].Width = new GridLength(Size2, GridUnitType.Star);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public void OnDragCompleted(DragCompletedEventArgs args)
        {
            switch (Direction)
            {
                case SplitConfig.EDirection.None:
                    break;
                case SplitConfig.EDirection.Horizontal:
                    Size1 = _grid.RowDefinitions[0].Height.Value;
                    Size2 = _grid.RowDefinitions[2].Height.Value;
                    break;
                case SplitConfig.EDirection.Vertical:
                    Size1 = _grid.ColumnDefinitions[0].Width.Value;
                    Size2 = _grid.ColumnDefinitions[2].Width.Value;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public SplitConfig BuildConfig(SplitConfig parent, Screen screen)
        {
            switch (Direction)
            {
                case SplitConfig.EDirection.None:
                    return SplitConfig.Leaf(parent, screen);
                case SplitConfig.EDirection.Horizontal:
                case SplitConfig.EDirection.Vertical:
                    var sc = new SplitConfig(Direction, Size1, Size2, parent, screen);
                    sc.A = Zone1.BuildConfig(sc, screen);
                    sc.B = Zone2.BuildConfig(sc, screen);
                    return sc;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void ReplaceWithSplit(ZoneViewModel zone, SplitConfig.EDirection direction)
        {
            Trace.Assert(Zone1 == zone || Zone2 == zone);
            if (Zone1 == zone)
            {
                Zone1 = direction == SplitConfig.EDirection.Horizontal
                    ? (ZoneViewModel)new HorizontalZoneViewModel(1, 1, -1, this)
                    : new VerticalZoneViewModel(1, 1, -1, this);
                Zone1.Zone1 = new LeafZoneViewModel(-1, Zone1);
                Zone1.Zone2 = new LeafZoneViewModel(-1, Zone1);
            }
            else if (Zone2 == zone)
            {
                Zone2 = direction == SplitConfig.EDirection.Horizontal
                    ? (ZoneViewModel)new HorizontalZoneViewModel(1, 1, -1, this)
                    : new VerticalZoneViewModel(1, 1, -1, this);
                Zone2.Zone1 = new LeafZoneViewModel(-1, Zone2);
                Zone2.Zone2 = new LeafZoneViewModel(-1, Zone2);
            }
        }

        public void ReplaceWith(ZoneViewModel replaced, ZoneViewModel newOne)
        {
            Trace.Assert(Zone1 == replaced || Zone2 == replaced);
            if (Zone1 == replaced)
                Zone1 = newOne;
            else if (Zone2 == replaced)
                Zone2 = newOne;
            newOne.Parent = this;
        }

        public void DeleteFirstZone()
        {
            Parent.ReplaceWith(this, Zone2);
        }

        public void DeleteSecondZone()
        {
            Parent.ReplaceWith(this, Zone1);
        }
    }

    public class LeafZoneViewModel : ZoneViewModel
    {
        public LeafZoneViewModel(int id, IParent parent)
            : base(id, SplitConfig.EDirection.None, -1, -1, parent)
        {
        }

        public void SplitHorizontal()
        {
            Parent.ReplaceWithSplit(this, SplitConfig.EDirection.Horizontal);
        }

        public void SplitVertical()
        {
            Parent.ReplaceWithSplit(this, SplitConfig.EDirection.Vertical);
        }
    }
    public class HorizontalZoneViewModel : ZoneViewModel
    {
        public HorizontalZoneViewModel(double size1, double size2, int id, IParent parent)
            : base(id, SplitConfig.EDirection.Horizontal, size1, size2, parent)
        {
        }
    }

    public class VerticalZoneViewModel : ZoneViewModel
    {
        public VerticalZoneViewModel(double size1, double size2, int id, IParent parent)
            : base(id, SplitConfig.EDirection.Vertical, size1, size2, parent)
        {
        }
    }
}