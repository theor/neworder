using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows.Controls.Primitives;
using Caliburn.Micro;
using NewOrder.Services;
using Ninject;

namespace NewOrder.ViewModels
{
    public class ShortcutHelp
    {
        public string Keys { get; private set; }
        public string Description { get; private set; }

        public ShortcutHelp(string keys, string description)
        {
            Keys = keys;
            Description = description;
        }
    }
    public class HelpViewModel : Screen
    {
        public BindableCollection<ShortcutHelp> ShortcutsHelp { get; set; }

        private HotKeyService HotKeyService;

        public HelpViewModel(HotKeyService hotKeyService)
        {
            HotKeyService = hotKeyService;
            ShortcutsHelp =
                new BindableCollection<ShortcutHelp>(
                    hotKeyService.Actions.Select(
                        a =>
                        {
                            var mi = a.Value.Method;
                            var desc = mi.GetCustomAttribute(typeof (DescriptionAttribute)) as DescriptionAttribute;
                            string description = desc != null ? desc.Description : "<unknown>";

                            return new ShortcutHelp(
                                string.Format("{0}+{1}", HotKeyService.Modifiers, a.Key.ToString()), description);
                        }));
        }



        public void Close()
        {
            this.TryClose();
        }

        //public void AttachView(object view, object context = null)
        //{
        //    var viewPopup = view as Popup;

        //}

        //public object GetView(object context = null)
        //{
        //}

        //public event EventHandler<ViewAttachedEventArgs> ViewAttached;
    }
}