using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using Caliburn.Micro;
using NewOrder.Services;

namespace NewOrder.ViewModels
{
    public class Command : ICommand
    {
        public string Header { get; set; }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            IoC.Get<ScreenViewService>().Load(Header);
        }

        public event EventHandler CanExecuteChanged;
    }

    public class PrimaryControlsViewModel : PropertyChangedBase
    {
        private string _CurrentProfileName;

        public string CurrentProfileName
        {
            get { return _CurrentProfileName; }
            set
            {
                if (value == _CurrentProfileName) return;
                _CurrentProfileName = value;
                NotifyOfPropertyChange();
            }
        }

        public void Save()
        {
            IoC.Get<ScreenViewService>().Save(null);
        }
        public void SaveAs()
        {
            var sfd = new SaveFileDialog{Filter = "json files (*.json)|*.json"};
            if (sfd.ShowDialog() != DialogResult.OK)
                return;
            IoC.Get<ScreenViewService>().Save(Path.GetFileNameWithoutExtension(sfd.FileName));
        }
        public BindableCollection<ICommand> LoadProfilesCommands { get; set; }

        public PrimaryControlsViewModel()
        {
            LoadProfilesCommands = new BindableCollection<ICommand>(ConfigSerialization.ListProfiles().Select(p => new Command{Header = p}));
            CurrentProfileName = IoC.Get<ScreenViewService>().Profile.Name;
        }

        public void Hide()
        {
            IoC.Get<ScreenViewService>().Hide();
        }
        public void Help()
        {
            IoC.Get<IWindowManager>().ShowPopup(IoC.Get<HelpViewModel>());
        }

        #region Config

        #endregion
    }

    public class LoadMessage
    {
        public readonly string ProfileToLoad;

        public LoadMessage(string profileToLoad = "def")
        {
            ProfileToLoad = profileToLoad;
        }
    }
}