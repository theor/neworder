﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;

namespace NewOrder.Hotkey
{
    public class Hotkey : IDisposable
    {
        public event EventHandler<HotkeyEventArgs> HotkeyPressed;
        protected virtual void OnHotkeyPressed(HotkeyInfo info)
        {
            if (HotkeyPressed == null) return;
            HotkeyPressed(this, new HotkeyEventArgs(this, info));
        }

        public Modifiers Modifier { get; private set; }
        public Keys Key { get; set; }
        public int Id { get; private set; }

        private readonly IntPtr _HWnd;
        private bool _Registered;

        public Hotkey(Modifiers modifier, Keys key, Window window, bool registerImmediately = false)
        {
            if (window == null) throw new ArgumentException("You must provide a form or window to register the hotkey to.", "window");
            Modifier = modifier;
            Key = key;
            _HWnd = new WindowInteropHelper(window).Handle;
            Id = GetHashCode();
            HookWndProc(window);
            if (registerImmediately) Register();
        }

        private void HookWndProc(Visual window)
        {
            var source = PresentationSource.FromVisual(window) as HwndSource;
            if (source == null) throw new HotkeyException("Could not create hWnd source from window.");
            source.AddHook(WndProc);
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            var info = HotkeyInfo.GetFromMessage(msg, lParam);
            if (info != null)
            {
                if (info.Key == Key && info.Modifier == Modifier) OnHotkeyPressed(info);
            }
            return IntPtr.Zero;
        }

        public void Register()
        {
            if (_Registered) return;
            if (!User32.RegisterHotKey(_HWnd, Id, (int)Modifier, (int)Key))
                throw new HotkeyException("Hotkey failed to register.");
            _Registered = true;
        }

        public void Unregister()
        {
            if (!_Registered) return;
            if (!User32.UnregisterHotKey(_HWnd, Id))
            {
                var wex = new Win32Exception();
                //if (wex.NativeErrorCode != 0) throw new HotkeyException("Hotkey failed to unregister. See InnerException for details.", wex);
            }
            _Registered = false;
        }

        public sealed override int GetHashCode()
        {
            return (int)Modifier ^ (int)Key ^ _HWnd.ToInt32();
        }

        public void Dispose()
        {
            Unregister();
            GC.SuppressFinalize(this);
        }

        ~Hotkey() { Unregister(); }
    }
}