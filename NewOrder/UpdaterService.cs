using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using NewOrder.Services;
using Squirrel;
using Action = System.Action;
using Timer = System.Timers.Timer;

namespace NewOrder
{
    public class UpdaterService
    {
        private const string ReleasesPath = @"http://new-order.github.io/Releases";
        public Version CurrentVersion { get; private set; }
        public string InstallPath { get; private set; }

        public UpdaterService()
        {
            using (var mgr = new UpdateManager(ReleasesPath))
            {
                CurrentVersion = mgr.CurrentlyInstalledVersion();
                InstallPath = mgr.RootAppDirectory;
            }
            Run(CheckForUpdate, TimeSpan.FromDays(1), CancellationToken.None);
        }
        public static async Task Run(Action action, TimeSpan period, CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await Task.Delay(period, cancellationToken);
                action();
            }
        }

        public async void CheckForUpdate()
        {
            try
            {
                if (CurrentVersion == null)
                    return;
                using (var mgr = new UpdateManager(ReleasesPath))
                {
                    CurrentVersion = mgr.CurrentlyInstalledVersion();

                    UpdateInfo updateInfo = await mgr.CheckForUpdate();
                    if (!updateInfo.ReleasesToApply.Any())
                        return;
                    if (
                        MessageBox.Show(string.Format("New version available: {0}\nCurrentlyInstalled: {1}\n Update app ?",
                            updateInfo.FutureReleaseEntry.Version, CurrentVersion), "Update available", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                        return;
                    IoC.Get<HotKeyService>().Dispose();

                    var up = await mgr.UpdateApp();
                    Trace.WriteLine(CurrentVersion);
                    Trace.WriteLine(up);
                }
                UpdateManager.RestartApp();
            }
            catch (Exception e)
            {
                File.AppendAllText("log.log", e.ToString());
            }
        }
    }
}