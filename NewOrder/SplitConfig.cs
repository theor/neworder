using System;
using System.Collections.Generic;
using System.Diagnostics;
using Caliburn.Micro;
using Newtonsoft.Json;
using Screen = System.Windows.Forms.Screen;

namespace NewOrder
{
    public class Profile
    {
        public Profile(string name, Dictionary<Screen, SplitConfig> configsByScreen)
        {
            Name = name;
            ConfigsByScreen = configsByScreen;
        }

        public string Name { get; set; }
        public Dictionary<Screen, SplitConfig>  ConfigsByScreen { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class SplitConfig
    {
        private static int _Id;
        public readonly int Id = _Id++;

        public enum EDirection { None, Horizontal, Vertical }
        public SplitConfig Parent { get; private set; }
        public Screen Screen { get; set; }

        [JsonProperty]
        public EDirection Direction;
        [JsonProperty]
        public double SizeA;
        [JsonProperty]
        public double SizeB;

        private SplitConfig _A;
        [JsonProperty]
        public SplitConfig A
        {
            get { return _A; }
            set
            {
                _A = value;
                if (value != null)
                    value.Parent = this;
            }
        }

        private SplitConfig _B;
        [JsonProperty]
        public SplitConfig B
        {
            get { return _B; }
            set
            {
                _B = value;
                if (value != null)
                    value.Parent = this;
            }
        }

        [JsonConstructor]
        public SplitConfig(SplitConfig parent, Screen screen)
        {
            Parent = parent;
            Screen = screen;
            Direction = EDirection.None;
        }

        public SplitConfig(EDirection direction, double sizeA, double sizeB, SplitConfig parent, Screen screen)
        {
            Parent = parent;
            Screen = screen;
            Direction = direction;
            SizeA = sizeA;
            SizeB = sizeB;
            CreateLeaves();
        }

        private void CreateLeaves()
        {
            A = Leaf(this, Screen);
            B = Leaf(this, Screen);
        }

        //public SplitConfig H(int sizeA, int sizeB)
        //{
        //    return new SplitConfig(EDirection.Horizontal, sizeA, sizeB, );
        //}

        //public static SplitConfig V(int sizeA, int sizeB)
        //{
        //    return new SplitConfig(EDirection.Vertical, sizeA, sizeB);
        //}

        public static SplitConfig Leaf(SplitConfig parent, Screen screen)
        {
            return new SplitConfig(parent, screen);
        }

        public SplitConfig Add(SplitConfig a, SplitConfig b)
        {
            A = a;
            B = b;
            a.CreateLeaves();
            b.CreateLeaves();
            return this;
        }

        public int Left
        {
            get
            {
                Trace.Assert(Parent == null || Parent.Direction != EDirection.None);
                if (Parent == null)
                    return Screen.WorkingArea.Left;
                switch (Parent.Direction)
                {
                    case EDirection.Horizontal:
                        return Parent.Left;

                    case EDirection.Vertical:
                        return Parent.Left + ((int)(Parent.ChildOffsetFactor(this) * Parent.Width));

                    default:
                        return -1;
                }
            }
        }

        public int Top
        {
            get
            {
                Trace.Assert(Parent == null || Parent.Direction != EDirection.None);
                if (Parent == null)
                    return Screen.WorkingArea.Top;
                switch (Parent.Direction)
                {
                    case EDirection.Horizontal:
                        return Parent.Top + ((int)(Parent.ChildOffsetFactor(this) * Parent.Height));

                    case EDirection.Vertical:
                        return Parent.Top;

                    default:
                        return -1;
                }
            }
        }

        public int Width
        {
            get
            {
                Trace.Assert(Parent == null || Parent.Direction != EDirection.None);
                if (Parent == null)
                    return Screen.WorkingArea.Width;
                switch (Parent.Direction)
                {
                    case EDirection.Horizontal:
                        return Parent.Width;

                    case EDirection.Vertical:
                        return ((int)(Parent.ChildSizeFactor(this) * Parent.Width));

                    default:
                        return 0;
                }
            }
        }

        public int Height
        {
            get
            {
                Trace.Assert(Parent == null || Parent.Direction != EDirection.None);
                if (Parent == null)
                    return Screen.WorkingArea.Height;
                switch (Parent.Direction)
                {
                    case EDirection.Horizontal:
                        return /*Parent.Top +*/ ((int)(Parent.ChildSizeFactor(this) * Parent.Height));

                    case EDirection.Vertical:
                        return Parent.Height;

                    default:
                        return -1;
                }
            }
        }

        public double ChildSizeFactor(SplitConfig child)
        {
            Trace.Assert(child == A || child == B);
            if (child == A)
                return (SizeA / (SizeA + SizeB));
            // child == B
            return (SizeB / (SizeA + SizeB));
        }

        public double ChildOffsetFactor(SplitConfig child)
        {
            Trace.Assert(child == A || child == B);
            if (child == A)
                return 0;
            // child == B
            return (SizeA / (SizeA + SizeB));
        }

        public override string ToString()
        {
            return string.Format("{0}: {1},{2},{3}, L{4} T{5} W{6} H{7}", Id, Direction, SizeA, SizeB, Left, Top, Width, Height);
        }
    }
}