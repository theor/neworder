using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using Caliburn.Micro;
using NewOrder.Services;
using NewOrder.ViewModels;
using Ninject;
using Screen = System.Windows.Forms.Screen;

namespace NewOrder {
    public class AppBootstrapper : BootstrapperBase {
        private IKernel _Kernel;
        public AppBootstrapper() {
            Initialize();
        }

        protected override void Configure() {
            _Kernel = new StandardKernel();

            _Kernel.Bind<IWindowManager>().To<WindowManager>().InSingletonScope();
            _Kernel.Bind<IEventAggregator>().To<EventAggregator>().InSingletonScope();

            _Kernel.Bind<NotifyIconService>().ToSelf().InSingletonScope();
            _Kernel.Bind<ScreenViewService>().ToSelf().InSingletonScope();
            _Kernel.Bind<UpdaterService>().ToSelf().InSingletonScope();

            _Kernel.Bind<Screen>().ToConstant(Screen.PrimaryScreen);
        }

        protected override object GetInstance(Type service, string key) {
            if (service == null)
                throw new ArgumentNullException("service");

            return _Kernel.Get(service);
        }

        protected override IEnumerable<object> GetAllInstances(Type service) {
            return _Kernel.GetAll(service);
        }

        protected override void BuildUp(object instance) {
            _Kernel.Inject(instance);
        }

        protected override async void OnStartup(object sender, StartupEventArgs e)
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(MyExceptionHandler);
            //IoC.Get<UpdaterService>().CheckForUpdate();
            IoC.Get<HotKeyService>().Start();
            IoC.Get<NotifyIconService>().Start();

            
        }

        private void MyExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            File.AppendAllText("log.log", e.ExceptionObject.ToString());
        }

        protected override void OnExit(object sender, EventArgs e)
        {
            _Kernel.Dispose();
            base.OnExit(sender, e);
        }
    }
}